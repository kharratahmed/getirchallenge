FROM node:12.16.1-alpine

RUN mkdir -p /usr/src/getirchallenge
WORKDIR /usr/src/getirchallenge

COPY *.json ./
COPY src /usr/src/getirchallenge/src
RUN npm install
RUN npm install pm2 -g
RUN npm run tsc
COPY .env /usr/src/getirchallenge/dist/.env
EXPOSE 8080
ENV NODE_ENV prod
CMD [ "npm","run","start:inDocker"]