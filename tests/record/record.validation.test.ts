import app from '../../src/app';
import * as request from 'supertest';
import { statusCode } from '../../src/utils/statusCode.utils';


describe('test record validation', () => {
    it('POST /api/v0/record Should respond with 422 && res.body.msg must be startDate required', async () => {
        const res = await request(app).post('/api/v0/record');
        expect(res.status).toBe(statusCode.ClientErrorBadRequest);
        expect(res.body.code).toBe(-1);
        expect(res.body.msg).toBe("startDate required");
    });

    it('POST /api/v0/record Should respond with 422 && res.body.msg must be invalid startDate value', async () => {
        const res = await request(app).post('/api/v0/record')
            .send({
                startDate: 'test',
            });
        expect(res.status).toBe(statusCode.ClientErrorBadRequest);
        expect(res.body.code).toBe(-1);
        expect(res.body.msg).toBe("invalid startDate value")
    });

    it('POST /api/v0/record Should respond with 422 && res.body.msg must be endDate required', async () => {
        const res = await request(app).post('/api/v0/record')
            .send({
                startDate: '1992-10-11',
            });
        expect(res.status).toBe(statusCode.ClientErrorBadRequest);
        expect(res.body.code).toBe(-1);
        expect(res.body.msg).toBe("endDate required")
    });

    it('POST /api/v0/record Should respond with 422 && res.body.msg must be invalid endDate value', async () => {
        const res = await request(app).post('/api/v0/record')
            .send({
                startDate: '1992-10-11',
                endDate: 'sdsd'
            });
        expect(res.status).toBe(statusCode.ClientErrorBadRequest);
        expect(res.body.code).toBe(-1);
        expect(res.body.msg).toBe("invalid endDate value")
    });

    it('POST /api/v0/record Should respond with 422 && res.body.msg must be endDate must be >= the startDate', async () => {
        const res = await request(app).post('/api/v0/record')
            .send({
                startDate: '1992-11-10',
                endDate: '1991-11-11'
            });
        expect(res.status).toBe(statusCode.ClientErrorBadRequest);
        expect(res.body.code).toBe(-1);
        expect(res.body.msg).toBe("endDate must be >= the startDate")
    });

    it('POST /api/v0/record Should respond with 422 && res.body.msg must be minCount required', async () => {
        const res = await request(app).post('/api/v0/record')
            .send({
                startDate: '1992-11-10',
                endDate: '1993-11-11'
            });
        expect(res.status).toBe(statusCode.ClientErrorBadRequest);
        expect(res.body.code).toBe(-1);
        expect(res.body.msg).toBe("minCount required")
    });

    it('POST /api/v0/record Should respond with 422 && res.body.msg must be invalid minCount', async () => {
        const res = await request(app).post('/api/v0/record')
            .send({
                startDate: '1992-11-10',
                endDate: '1993-11-11',
                minCount: -1
            });
        expect(res.status).toBe(statusCode.ClientErrorBadRequest);
        expect(res.body.code).toBe(-1);
        expect(res.body.msg).toBe("invalid minCount")
    });

    it('POST /api/v0/record Should respond with 422 && res.body.msg must be maxCount required', async () => {
        const res = await request(app).post('/api/v0/record')
            .send({
                startDate: '1992-11-10',
                endDate: '1993-11-11',
                minCount: 10
            });
        expect(res.status).toBe(statusCode.ClientErrorBadRequest);
        expect(res.body.code).toBe(-1);
        expect(res.body.msg).toBe("maxCount required")
    });

    it('POST /api/v0/record Should respond with 422 && res.body.msg must be invalid maxCount', async () => {
        const res = await request(app).post('/api/v0/record')
            .send({
                startDate: '1992-11-10',
                endDate: '1993-11-11',
                minCount: 10,
                maxCount: "dsdds"
            });
        expect(res.status).toBe(statusCode.ClientErrorBadRequest);
        expect(res.body.code).toBe(-1);
        expect(res.body.msg).toBe("invalid maxCount")
    });

    it('POST /api/v0/record Should respond with 422 && res.body.msg must be maxCount must be >= minCount', async () => {
        const res = await request(app).post('/api/v0/record')
            .send({
                startDate: '1992-11-10',
                endDate: '1993-11-11',
                minCount: 10,
                maxCount: 1
            });
        expect(res.status).toBe(statusCode.ClientErrorBadRequest);
        expect(res.body.code).toBe(-1);
        expect(res.body.msg).toBe("maxCount must be >= minCount")
    });
});