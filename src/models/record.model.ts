import { model, Schema } from "mongoose";

const recordsSchema  = new Schema({
    key: { type: String, required: true },  // mybe must be unique and default random string
    value: { type: String, required: true }, // mybe must be unique and default random string
    createdAt: { type: Date, default: Date.now, index: true },
    count: { type: [Number], default: [] }
});

export default model('records', recordsSchema );