import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import recordRoute from './record.route';
import { notFoundErrorResponse } from '../utils/response.utils';
import * as swagger from 'swagger-ui-express';
import * as swaggerConfig from '../../swagger.json'

const corsOptions = {
    origin: '*',
    methods: "POST",
};

export default function appRoutes(app) {
    app.use(cors(corsOptions));
    app.use(bodyParser.json());
    app.get('/ping', async (_req, res) => {
        res.send('PONG GETIR TEST!');
    });
    app.use('/api/v0/docs', swagger.serve, swagger.setup(swaggerConfig));
    app.use('/api/v0/record', recordRoute);
    app.use('*', async (req, res) => {
        return notFoundErrorResponse(res);
    });
}
